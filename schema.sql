
CREATE TABLE groups (
    gid  SERIAL NOT NULL PRIMARY KEY  ,
    name character varying,
    contribution_amount integer NOT NULL,
    group_size integer NOT NULL,
    creation_date timestamp with time zone DEFAULT now(),
    round_duration integer NOT NULL,
    creator character varying NOT NULL,
    account_number character varying(10) NOT NULL
);


CREATE TABLE members (
    mid SERIAL NOT NULL PRIMARY KEY ,
    group_id INTEGER NOT NULL ,
    account_id character varying(10) NOT NULL,
    customer_id character varying NOT NULL,
    name character varying,
    rating double precision,
    date_added timestamp with time zone DEFAULT now(),
    FOREIGN KEY (group_id) REFERENCES groups(gid) ON DELETE CASCADE
);


