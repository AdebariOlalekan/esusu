package main

import (
	"encoding/json"
	"esusu/src/models/group"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"strconv"
)

func PatchGroup(w http.ResponseWriter, r *http.Request) {

	uuid := w.Header().Get("X-Correlation-ID")
	logfields := log.Fields{
		"uuid":    uuid,
		"Handler": "patchGroup",
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithFields(logfields).Errorf("Could not read the request body : %s", err.Error())
		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = fmt.Fprint(w, "")
		return
	}

	erro := r.Body.Close()
	if err != nil {
		log.WithFields(logfields).Errorf("can't close request: %s", erro.Error())

		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "")
		return
	}
	var grp group.Group
	err = json.Unmarshal(body, &grp)
	if err != nil {
		log.WithFields(logfields).Errorf("Unable to parse request body : %s ", err.Error())
		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "")
		return
	}
	gid, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		log.WithFields(logfields).Errorf("invalid: group id must be an integer")

		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, "Group id must be an integer")
		return
	}
	grp.GID = uint(gid)
	patchgrp := group.PatchGroup(int(grp.GID), grp, uuid)
	if patchgrp {
		log.WithFields(logfields).Infof("patch group id: %d - success", grp.GID)

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)

		fmt.Fprint(w, "successfully updated the group details")
	}else {
		log.WithFields(logfields).Infof("patch group - failed")

		w.Header().Set("Content-Type", "text/plain")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusBadRequest)

		responseBody := "group not found"
		fmt.Fprint(w, responseBody)

	}


}