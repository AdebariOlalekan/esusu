package main

import (
	"esusu/src/misc"
	"esusu/src/models/group"
	"esusu/src/models/member"

	"fmt"
	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	_ "github.com/lib/pq"
	"net/http"
	"os"
	"path/filepath"
	"runtime/trace"
	"time"
	"ubanquity.com/libraries/lbsql"
	"ubanquity.com/libraries/util"
)

var Db *lbsql.DB

const STATIC_CONFIG = "../config/esusu_static.toml"
const DYNAMIC_CONFIG = "../config/esusu_dynamic.toml"

var sconf StaticConfig
var dconf DynamicConfig

type defaults struct {
	ServiceName              string
	DynamicConfigRefreshRate int
}

type StaticConfig struct {
	Default defaults
	Server  serverConfiguration
	Log     util.LogsConfiguration
	DB      lbsql.DbConfiguration
	Metrics metricsConfiguration
}

type DynamicConfig struct {
	LogLevel string
	AuthEngineToken string
}
func DefaultDynamicConfig() *DynamicConfig {
	return &DynamicConfig{
	LogLevel: "debug",
	}

}

type metricsConfiguration struct {
	Host         string
	Key          string
	Organization string
	Site         string
	Node         string
	Container    string
	Delta        int
}


func DefaultStaticConfig() *StaticConfig {
	return &StaticConfig{
		Default: defaults{
			ServiceName:              "Esusu",
		},
		Server: serverConfiguration{
			Port:    8000,
			Version: "",
		},
		Log: util.LogsConfiguration{
			Console:         true,
		},
		DB: lbsql.DbConfiguration{
			Url:          "postgres://postgres:postgres@127.0.0.1:5432/esusu?sslmode=disable&connect_timeout=30",
			ExtraParams:  "&statement_timeout=30000&lock_timeout=10000",
			ShadowParams: "?sslmode=disable&connect_timeout=1&read_timeout=1000&write_timeout=1000",
			RefreshRate:  1,
			Driver: "postgres",
		},
		Metrics: metricsConfiguration{
			Host:         "dashboard.ubanquity.io:2003",
			Key:          "",
			Organization: "ubanquity",
			Site:         "lab",
			Node:         "",
			Delta:        60,
			Container:    "esusu",
		},
	}
}

type serverConfiguration struct {
	Port    int
	Version string
}


func init() {

	go func() {
		log.Println(http.ListenAndServe("127.0.0.1:6060", nil))
	}()

	// Temporary debug level
	log.SetLevel(log.DebugLevel)

	// Start tracing
	f, err := os.Create("trace.out")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	err = trace.Start(f)
	if err != nil {
		panic(err)
	}
	defer trace.Stop()


	// Parse the static configuration file
	sconf = *DefaultStaticConfig()

	if _, err := toml.DecodeFile(filepath.FromSlash(STATIC_CONFIG), &sconf); err != nil {
		log.Fatalf("can't read static configuration: %s", err)

	}
	log.Infof("Load static configuration: success")

	// Parse the dynamic configuration file
	dconf = *DefaultDynamicConfig()
	if _, err := toml.DecodeFile(filepath.FromSlash(DYNAMIC_CONFIG), &dconf); err != nil {
			log.Fatalf("can't read dynamic configuration: %s", err)

	}
	log.Infof("Load dynamic configuration: success")


	//Logs configuration
	logconf := util.LogsConfiguration(sconf.Log)
	util.SetupLogsConfiguration(logconf, dconf.LogLevel)


	// Metrics configuration
	metricsconf := util.MetricsConfiguration(sconf.Metrics)
	util.SetupMetricsConfiguration(metricsconf)

	// Server version configuration
	util.ServerVersion(sconf.Server.Version)

	jsonSConf, _ := util.JsonMarshal(sconf, false)
	jsonDConf, _ := util.JsonMarshal(dconf, false)

	log.Debugf("Static configuration\n%+v", string(jsonSConf))
	log.Debugf("Dynamic configuration\n%+v", string(jsonDConf))



	// Reload the dynamic configuration
	go func() {
		for {
			if _, err := toml.DecodeFile(filepath.FromSlash(DYNAMIC_CONFIG), &dconf); err != nil {
					log.Fatalf("can't read dynamic configuration: %s", err)
			}

			if level, err := log.ParseLevel(dconf.LogLevel); err == nil {
				log.SetLevel(level)
			}
			time.Sleep(time.Duration(sconf.Default.DynamicConfigRefreshRate) * time.Second)
		}
	}()
	// prepare database connection
	Db, err = lbsql.DbConfigure(sconf.DB, "esusu")
	if err != nil {
		log.Fatalf("Can't connect to database: %s", err)
	}
	log.Infof("Connected to DB: %v", Db.Conn)
	group.PreparedStatements(Db)
	member.PreparedStatements(Db)

}

func Middlewaress(h http.Handler) http.Handler {
	return alice.New(util.UUIDHandler, util.ServerTagHandler, util.HTTPLoggingHandler, misc.Authentication).Then(h)

}
func main() {
	router := mux.NewRouter().StrictSlash(true)


	router.HandleFunc("/esusu/groups/create/", PostGroup).Methods("POST")
	router.HandleFunc("/esusu/groups", GetGroups).Methods("GET")
	router.HandleFunc("/esusu/group", GetGroup).Methods("GET")
	router.HandleFunc("/esusu/groups/{id}/remove/", DeleteGroup).Methods("DELETE")
	router.HandleFunc("/esusu/groups/{id}/edit/", PatchGroup).Methods("PATCH")
	router.HandleFunc("/esusu/members/add/", PostMember).Methods("POST")
	router.HandleFunc("/esusu/members/", GetMembers).Methods("GET")
	router.HandleFunc("/esusu/member/{id}", GetMember).Methods("GET")
	router.HandleFunc("/esusu/members/{id}/remove/", DeleteMember).Methods("DELETE")

	middlewares := Middlewaress(router)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", sconf.Server.Port), middlewares); err != nil {
		log.Errorf("couldnt't listen and serve %s", err.Error())
	}
}


