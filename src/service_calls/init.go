package service_calls

import (
	"github.com/BurntSushi/toml"
	"log"
	"path/filepath"
)

const STATIC_CONFIG  = "../config/esusu_dynamic.toml"

type staticConfig struct {
	RequestsConfigs requestsConfigs
}
type requestsConfigs struct {
	AuthEngineUrl string
	InfoBankingUrl string
	SystemToken string
}

var sconf staticConfig
var requestConfig requestsConfigs

func init(){
	go func() {
		if _, err := toml.DecodeFile(filepath.FromSlash(STATIC_CONFIG), &sconf); err != nil {
			log.Fatalf("can't read static configuration: %s", err.Error())
		}
		requestConfig.SystemToken = sconf.RequestsConfigs.SystemToken
		requestConfig.AuthEngineUrl = sconf.RequestsConfigs.AuthEngineUrl
		requestConfig.InfoBankingUrl =  sconf.RequestsConfigs.InfoBankingUrl
	}()
}
