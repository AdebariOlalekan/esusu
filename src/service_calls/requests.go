package service_calls

import (
	"encoding/json"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"time"

)

//GetUserAccount : fetches the account of the users full details from infobanking/account. it
// returns a map[string]interface{} and error
func GetUserAccount(accountNumber string, uuid string) (map[string]interface{} , error) {
	logfield := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Get User Account",
	}
	accounts, erro := GetUserAccounts(accountNumber, uuid)
	if erro != nil {
		log.WithFields(logfield).Errorf("Unable to prepare request: %s", erro.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : erro.Error(),
		}
		return resp, erro
	}
	if len(accounts) < 1 {
		log.WithFields(logfield).Errorf("Unable to get the account associated with the account number")
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : "Invalid Account number: The entered account number does not exist",
		}
		return resp, errors.New("Invalid Account number: The entered account number does not exist")
	}
	var account  map[string]interface{}
	account = accounts[0]


	timeout := time.Duration(300 * time.Second)

	client := http.Client{
		Timeout: timeout,
	}
	request, err := http.NewRequest("GET", fmt.Sprintf("%saccount/%s", requestConfig.InfoBankingUrl, account["account_id"]), nil )
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", requestConfig.SystemToken))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("X-Correlation-Id", uuid)
	log.WithFields(logfield).Infof("calling Infobanking with: %s", request.URL.String())

	if err != nil {
		log.WithFields(logfield).Errorf("Unable to prepare request: %s", err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}

	resp, er := client.Do(request)
	if er != nil {
		log.WithFields(logfield).Errorf("failed to get user account from the account number specified : %s", err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}
	 var jsonResp  map[string]interface{}
	respBody, erro := ioutil.ReadAll(resp.Body)
	if erro != nil {
		log.WithFields(logfield).Errorf("Error while reading response body : %s", erro.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}
	err  = json.Unmarshal([]byte(respBody), &jsonResp)
	if  err != nil {
		log.WithFields(logfield).Errorf("Error while reading respomnse body : %s", erro.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}
	log.WithFields(logfield).Infof("Response body from customer-aaa ", jsonResp)

	return jsonResp, nil
}

// GetUserAccounts gets the list of accounts that a customer have from infobanking/customer_account
func GetUserAccounts( accountNo string, uuid string) ([]map[string]interface{}, error)  {
	logfields := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Get User Accounts list",
	}
	timeout := 300 * time.Duration(time.Second)
	client := http.Client{
		Timeout : timeout,
	}
	request, err := http.NewRequest("GET", fmt.Sprintf("%scustomer_account", requestConfig.InfoBankingUrl), nil)
	if err != nil {
		log.WithFields(logfields).Errorf("Unable to prepare request : %s", err)
		empty := make([]map[string]interface{}, 0)
		return empty , err
	}
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", requestConfig.SystemToken))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("X-Correlation-Id", uuid)
	queryParam := request.URL.Query()
	queryParam.Add("account_no", accountNo)
	request.URL.RawQuery = queryParam.Encode()
	log.WithFields(logfields).Infof("Calling infobanking with : %s", request.URL.String())
	resp, erro := client.Do(request)
	if erro != nil {
		log.WithFields(logfields).Errorf("Unable to make request: %s", erro)
		empty := make([]map[string]interface{}, 0)
		return empty , erro
	}
	var jsonResp []map[string]interface{}

	respBody, errr := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.WithFields(logfields).Errorf("Unable to read Response request: %s", errr)
		empty := make([]map[string]interface{}, 0)
		return empty , errr
	}
	if er := json.Unmarshal([]byte(respBody), &jsonResp); er != nil {
		log.WithFields(logfields).Errorf("Unable to Unmarshal response to map: %s", er)
		empty := make([]map[string]interface{}, 0)
		return empty , er
	}
	log.WithFields(logfields).Infof("Response body from infobanking ", jsonResp)

	return jsonResp, nil

}
