package main

import (
	"encoding/json"
	"errors"
	"esusu/src/misc"
	"esusu/src/models/group"
	"esusu/src/models/member"
	"esusu/src/service_calls"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"ubanquity.com/libraries/lbsql"
)

func validateMember(nmember *member.Member, uuid string, r *http.Request,db *lbsql.DB ) (map[string]interface{}, error) {
	logfields := log.Fields{
		"uuid" : uuid,
		"handler" : "Post Group",
	}
	if nmember.GroupID < 1 || nmember.AccountID == "" {
		log.WithFields(logfields).Errorf("Incomplete Member registeration data provided please review. : Account_id and group_id are compulsary")
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : "Incomplete Member registeration data please review. : Account_id and group_id are compulsary",
		}
		return resp, errors.New("Incomplete Member registeration data please review. : Account_id and group_id are compulsary")
	}
	account, err := service_calls.GetUserAccount(nmember.AccountID, uuid)
	if err != nil {
		log.WithFields(logfields).Errorf(err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}
	accountBalance := account["balance_available"]
	if accountBalance.(float64) <= 0 {
		log.WithFields(logfields).Errorf(fmt.Sprintf("Failed, the inputed account doesnt have sufficient balance : %d", accountBalance.(int64)))
		resp := map[string]interface{}{
			"status":  "failed",
			"message": fmt.Sprintf("Failed, the inputed account doesnt have sufficient balance : %d", accountBalance.(int64)),
		}
		return resp, err
	}
	userQuery2 := group.FilterQuery{
		Asc: true,
		SortBy: "name",
	}
	grp := group.SelectAllGroups(db, &userQuery2, uuid)
	groupExists := false
	for i := 0; i < len(grp); i++ {
		if grp[i].GID == nmember.GroupID  {
			groupExists = true
			break
		}
	}
	if !groupExists {
		resp := map[string]interface{}{
			"status":  "failed",
			"message": fmt.Sprintf("The group You are trying to add doesnt exist"),
		}
		return resp, errors.New("The Specified Group Does not exists")
	}
	customer_details := account["customer"].(map[string]interface{})
	return map[string]interface{}{
		"customer_id" : account["customer_id"].(string),
		"name" : fmt.Sprintf("%s %s", customer_details["first_name"].(string), customer_details["last_name"].(string)),
	} , nil
}


func PostMember(w http.ResponseWriter, r *http.Request) {
	uuid := r.Header.Get("X-Correlation-ID")
	logfields := log.Fields{
		"uuid" : uuid,
		"Handler" : "PostMember",
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithFields(logfields).Errorf("unable to read request body : %s", err.Error())
		misc.Response(w, "unable to read request body", logfields, http.StatusInternalServerError)
	}
	var mbr member.Member
	err = json.Unmarshal(body, &mbr)
	if err != nil {
		log.WithFields(logfields).Errorf("Could not decode request body: %s ", err.Error())
		misc.Response(w,"Could not decode request body", logfields, http.StatusInternalServerError)
	}
	log.WithFields(logfields).Info("Posted data :", mbr)
	mem, err := validateMember(&mbr, uuid, r, Db)
	if err != nil {
		log.WithFields(logfields).Errorf("The member you want to register is not valid or is not eligible : %s ", err.Error())
		misc.Response(w, err.Error(), logfields, http.StatusBadRequest)
		return
	}
	//TODO remember to make sure one member would never be added to a group twice
	mbr.CustomerID = mem["customer_id"].(string)
	mbr.Name = mem["name"].(string)
	nmember, err := mbr.AddMember(Db, uuid)

	if err != nil {
		log.WithFields(logfields).Errorf("FAILED: couldn't add Member")
		responseBody := fmt.Sprintf("Can not create member : %s", err.Error())
		misc.Response(w,responseBody,logfields, http.StatusBadRequest)
		return
	}
	misc.Response(w,nmember,logfields, http.StatusCreated)
	return

}
