package main

import (
	"esusu/src/misc"
	"esusu/src/models/group"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
)

func DeleteGroup(w http.ResponseWriter, r *http.Request) {
	uuid := w.Header().Get("X-Correlation-ID")

	customerID := strings.Split(r.Context().Value("user").(string), "/")[0]

	logFields := log.Fields{
		"uuid":    uuid,
		"handler": "delete group",
	}

	if err := r.Body.Close(); err != nil {
		log.WithFields(logFields).Errorf("can't close request: %s", err.Error())
		misc.Response(w, "",logFields,http.StatusInternalServerError)
		return
	}

	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		log.WithFields(logFields).Errorf("illegal non integer id")
		misc.Response(w, fmt.Sprintf("illegal non integer id : %s", string(id)),logFields,http.StatusBadRequest)
	}

	log.WithFields(logFields).Infof("delete group id: %d", id)

	groups, erro := group.DeleteGroup(uint(id), uuid, customerID)
	if erro != nil {
		log.WithFields(logFields).Infof("delete group id: %d - not found", id)
		responseBody := "Group not found"
		misc.Response(w, responseBody, logFields, http.StatusBadRequest)
		return
	}

	log.WithFields(logFields).Infof("Groups affected by delete: %d", groups)

	misc.Response(w, fmt.Sprintf("Affected Groups : %d", groups), logFields, http.StatusOK)
	return
}

