package main

import (
	"encoding/json"
	"esusu/src/misc"
	"esusu/src/models/group"
	"esusu/src/models/member"
	"esusu/src/service_calls"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"strings"
	"ubanquity.com/libraries/util"
)

// TODO this validation function will be the one to handle validatoion of the person trying to create the group
func validateCreator(group *group.Group, uuid string, r *http.Request)  (map[string]interface{}, error) {
	logfields := log.Fields{
		"uuid" : uuid,
		"handler" : "Post Group",
	}
	userDetails := fmt.Sprintf("%v", r.Context().Value("user"))
	userID := strings.Split(userDetails, "/")[0]
	account, err := service_calls.GetUserAccount(group.AccountNumber, uuid)
	if err != nil {
		log.WithFields(logfields).Errorf(err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}
	//check if the customer_id in token is equal to then on recieved from getting the details from infobanking
	if userID != account["customer_id"].(string){
		log.WithFields(logfields).Errorf("Invalid token or Account number: either you \n entered a wrong customer id or wrong customer token pls check")
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : "Invalid token or Account number: either you \n entered a wrong customer id or wrong customer token pls check",
		}
		return resp, errors.New("Invalid token or Account number: either you \n entered a wrong customer id or wrong customer token pls check")
	}
	// check if the balance on account is greater than a thresshold; we shall edit this later
	accountBalance := account["balance_available"]
	if accountBalance.(float64) <= 0 {
		log.WithFields(logfields).Errorf(fmt.Sprintf("Failed, the inputed account doesnt have sufficient balance : %d", accountBalance.(int64)))
		resp := map[string]interface{}{
			"status":  "failed",
			"message": fmt.Sprintf("Failed, the inputed account doesnt have sufficient balance : %d", accountBalance.(int64)),
		}
		return resp, err
	}
	customer_details := account["customer"].(map[string]interface{})
	return map[string]interface{}{
		"customer_id" : account["customer_id"].(string),
		"name" : fmt.Sprintf("%s %s", customer_details["first_name"].(string), customer_details["last_name"].(string)),
	} , nil
}
func checkParams(grp *group.Group) error {

	if len(grp.AccountNumber) != 10 {
		return errors.New("You must enter an Account Number which must be equal to 10 digits")
	}
	if grp.Name == "" {
		grp.Name = fmt.Sprintf("Esusu-%s", util.GenerateRandomStringWithDefaultChars(5))
	}
	return nil
}

func PostGroup(w http.ResponseWriter, r *http.Request)  {
	uuid := r.Header.Get("X-Correlation-ID")

	logfields := log.Fields{
		"uuid" : uuid,
		"handler" : "Post Group",
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.WithFields(logfields).Errorf("Request cannot be closed: %s", err.Error())
		misc.Response(w, "",logfields, http.StatusInternalServerError)
		return
	}

	var ngroup group.Group

	err = json.Unmarshal(body, &ngroup)
	if err != nil {

		log.WithFields(logfields).Errorf("The request body cannot be read : %s ", err.Error())
		misc.Response(w,"The request body could not be converted to struct",logfields, http.StatusInternalServerError)
		return
	}
	if err = checkParams(&ngroup); err != nil {
		log.WithFields(logfields).Error(err.Error())
		misc.Response(w, err.Error(), logfields, http.StatusBadRequest)
		return
	}
	customer , er := validateCreator(&ngroup, uuid, r)
	if er != nil {
		log.WithFields(logfields).Errorf("There was an error during validating the group creation : %s \n stack trace below %s ",er.Error())
		misc.Response(w,"The request body could not be read", logfields, http.StatusBadRequest)
		return
	}
	ngroup.Creator = customer["customer_id"].(string)

	ngroup, err = ngroup.AddGroup(Db, uuid)
	if err != nil {
		log.WithFields(logfields).Errorf("FAILED: couldn't add Group")
		responseBody := "Can not create group : " + err.Error()
		misc.Response(w,responseBody,logfields, http.StatusBadRequest)
		return
	}
	nmember := member.Member{
		AccountID: ngroup.AccountNumber,
		GroupID: ngroup.GID,
		Name : customer["name"].(string),
		Rating  : 0.0,
		CustomerID : customer["customer_id"].(string),
	}
	gmember, eri := ngroup.AddMember(Db, nmember, uuid)
	if eri != nil {
		log.WithFields(logfields).Errorf("FAILED: Could not add the member to the group: %s", eri.Error())
		log.WithFields(logfields).Error("Rolling back group creation due to inablility to create a member for the group creator")
		_, err := group.DeleteGroup(ngroup.GID, uuid, customer["customer_id"].(string))
		if err != nil {
			log.WithFields(logfields).Error("Rolling back Group creation :  Unable to delete created group")
		}
		misc.Response(w, gmember, logfields, http.StatusBadRequest)
		return
	}
	var to_return map[string]interface{}
	if len(gmember) >= 1 {
		to_return = map[string]interface{}{
			"group details": ngroup,
			"Membership details": gmember,
		}
		log.WithFields(logfields).Info("returned data from group creation", to_return)
		misc.Response(w, to_return,logfields, http.StatusCreated)
		return
	}
	log.WithFields(logfields).Info("returned data from group creation", ngroup)
	misc.Response(w, ngroup, logfields, http.StatusCreated)
	return

// TODO we need to notify the group creator about the success of the group creation
}