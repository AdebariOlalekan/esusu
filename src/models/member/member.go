package member

import (
	log "github.com/Sirupsen/logrus"
	"time"
	"ubanquity.com/libraries/lbsql"
)

type Member struct {
	AccountID string `json:"account_id"`
	GroupID	  uint `json:"group_id"`
	Name string `json:"name"`
	MID uint64 `json:"mid"`
	Rating float64 `json:"rating"`
	DateAdded time.Time `json:"date_added"`
	CustomerID string `json:"customer_id"`
}

type FilterQuery struct {
	Offset        int64  `json:"offset"`
	Limit         int64  `json:"limit"`
	SortBy        string `json:"sortby"`
	Asc           bool   `json:"asc"`
	Rating        float64 `json:"rating"`
	GroupID		  int64 `json:"group_id"`
	//DateAdded time.Time `json:"date_added"`
}

var InsertMemberStatement string
var SelectMemberStatement *lbsql.Stmt
var SelectAllMembersStmt string
var SelectAllMembersByRatingStmt string
var DeleteMemberStmt *lbsql.Stmt
var UpdateMemberStmt *lbsql.Stmt
var SelectMemberByGroup string

func PreparedStatements(db *lbsql.DB)  {
	log.Infof("Preparing statements for Member")
	var err error

	InsertMemberStatement = `INSERT INTO members("account_id", "group_id", "name","rating", "customer_id") VALUES ($1,$2, $3, $4, $5) returning mid;`

	SelectMemberStatement, err = db.Prepare(`SELECT * FROM members WHERE mid=$1;`)
	if err != nil {
		log.Errorf("can't prepare select member statement: %s", err.Error())
	}

	SelectAllMembersStmt = `SELECT *  FROM members ORDER BY %s %s  OFFSET %d`
	//SelectAllMembersStmt = `SELECT *  FROM members`
	SelectAllMembersByRatingStmt = `SELECT *  FROM members WHERE rating='%f' ORDER BY %s %s  OFFSET %d`

	DeleteMemberStmt, err = db.Prepare(`DELETE FROM members WHERE mid=$1;`)
	if err != nil {
		log.Errorf("can't prepare delete member statement: %s", err.Error())
	}
	SelectMemberByGroup = `SELECT *  FROM members WHERE group_id='%d' ORDER BY %s %s  OFFSET %d`

	// TODO am not sure if we can or need to update a member
	//UpdateMemberStmt, err = db.Prepare(`UPDATE members SET name=coalesce($2, name), contribution_amount=coalesce($3, contribution_amount), member_size=coalesce($4, member_size), round_duration=coalesce($5, round_duration) WHERE mid=$1;`)
	//if err != nil {
	//	log.Errorf("can't prepare update member statement: %s", err.Error())
	//}

}

func CloseStatements() {
	log.Infof("closing sql statements")
	_ = SelectMemberStatement.Close()
	_ = DeleteMemberStmt.Close()
	_ = UpdateMemberStmt.Close()
}



