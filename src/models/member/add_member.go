package member

import (
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"ubanquity.com/libraries/lbsql"
)


func (member Member)AddMember(db *lbsql.DB, uuid string) (Member, error) {
	logFields := log.Fields{
		"uuid":     uuid,
		"function": "sql",
	}
	userQuery := FilterQuery{
		GroupID: int64(member.GroupID),
	}
	members := SelectAllMembers(db, &userQuery, uuid)

	exists := false
	for i := 0; i < len(members); i++ {
		if members[i].CustomerID == member.CustomerID{
			exists = true
			break
		}

	}
	if exists {
		emptyMember := Member{}
		return emptyMember, errors.New("Member Already exists on this group")
	}
	tranx, err :=  db.Begin()

	if err != nil {
		log.WithFields(logFields).Errorf("transaction failed to start : %s", err.Error())

		//return empty member
		emptyMember := Member{}

		return emptyMember, err
	}
	var mid uint64
	err = db.QueryRow(InsertMemberStatement, member.AccountID,member.GroupID, member.Name, member.Rating, member.CustomerID).Scan(&mid)
	if err != nil {
		log.WithFields(logFields).Errorf("Failed to insert member : %s", err.Error())

		tranx.Rollback()
		emptyMember := Member{}

		return emptyMember, err
	}

	if mid < 1 {
		log.WithFields(logFields).Errorf("Unable to get the added member's id, so we rollback the transaction ")

		tranx.Rollback()
		emptyMember := Member{}
		return emptyMember, errors.New("Unable to add member")
	}
	member.MID = mid
	tranx.Commit()
	return member, nil
}
