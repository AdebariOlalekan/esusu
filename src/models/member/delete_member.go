package member

import (
	log "github.com/Sirupsen/logrus"
)

func DeleteMember(id uint64, uuid string) int64{

	LogFields := log.Fields{
		"uuid":     uuid,
		"Function": "sql",
	}

	rowsAffected , err := DeleteMemberStmt.Exec(id)
	if err != nil {
		log.WithFields(LogFields).Errorf("Couldn't Delete member : %s ", err.Error())
		return 0
	}

	affectedRows, err := rowsAffected.RowsAffected()

	if err != nil {
		log.WithFields(LogFields).Errorf("Could not count the affected rows : % ", err.Error())
		return 0
	}
	return  affectedRows


}

