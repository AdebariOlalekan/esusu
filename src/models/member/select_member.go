package member

import (
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"ubanquity.com/libraries/lbsql"
)

func SelectMember(id uint64, uuid string) (Member , error) {

	logfields := log.Fields{

		"uuid":     uuid,
		"function": "sql",
	}

	rows, err := SelectMemberStatement.Query(id)

	if err != nil {

		log.WithFields(logfields).Errorf("cannot get the required member: %s", err.Error())

		return Member{}, err
	}

	defer rows.Close()

	var member Member
	for rows.Next() {
		if err := rows.Scan(&member.MID, &member.GroupID, &member.AccountID,&member.CustomerID, &member.Name,  &member.Rating, &member.DateAdded ); err != nil {
			log.WithFields(logfields).Errorf("can not map rows to their respective values: %s", err.Error())
			return Member{}, err
		}

	}
	if member.CustomerID == "" && member.GroupID == 0 && member.MID == 0 {
		log.WithFields(logfields).Errorf("member Not Found with ID : %d", id)
		return Member{}, errors.New(fmt.Sprintf("member Not Found with ID : %d", id))
	}
	return member, nil
}

func SelectAllMembers(db *lbsql.DB, userQuery *FilterQuery, uuid string) []Member {
	logfields := log.Fields{
		"uuid":     uuid,
		"function": "sql",
	}


	if userQuery.Rating == 0.0 {
		userQuery.Rating = 0.0
	}

	var order  = "ASC"
	if !userQuery.Asc {
		order = "DESC"
	}

	if userQuery.SortBy == ""{
		userQuery.SortBy = "name"
	}
	var statement string
	if userQuery.Rating >= 0.1 {
		statement = fmt.Sprintf(SelectAllMembersByRatingStmt,userQuery.Rating, userQuery.SortBy, order, userQuery.Offset)
	}else if userQuery.GroupID > 0 {
		statement = fmt.Sprintf(SelectMemberByGroup,userQuery.GroupID, userQuery.SortBy, order, userQuery.Offset )
	}else {
		statement = fmt.Sprintf(SelectAllMembersStmt, userQuery.SortBy, order, userQuery.Offset)
	}
	//if userQuery.Rating <= 0.0 {
		rows, err := db.Query(statement)
		log.WithFields(logfields).Infof("Querying members with sql  : %s", statement)
		if err != nil {
			log.WithFields(logfields).Errorf("could not get members from database : %s", err.Error())
			empty := make([]Member, 0)
			return empty
		}

		var members []Member
		for rows.Next() {
			mbr := new(Member)
			err = rows.Scan(&mbr.MID, &mbr.GroupID, &mbr.AccountID, &mbr.CustomerID, &mbr.Name, &mbr.Rating, &mbr.DateAdded,  )
			if  err != nil {
				log.WithFields(logfields).Errorf("could not map rows to their values: %s", err.Error())
			} else {
				members = append(members, *mbr)
				log.WithFields(logfields).Println("returned members : ", members)
			}
		}

		return members
	//}else {
	//	rows, err := db.Query(fmt.Sprintf(SelectAllMembersByRatingStmt,userQuery.Rating, userQuery.SortBy, order, userQuery.Offset))
	//	log.WithFields(logfields).Infof("Querying members with sql  : %s", fmt.Sprintf(SelectAllMembersByRatingStmt,userQuery.Rating, userQuery.SortBy, order, userQuery.Offset))
	//	if err != nil {
	//		log.WithFields(logfields).Errorf("could not get members from database : %s", err.Error())
	//		empty := make([]Member, 0)
	//		return empty
	//	}
	//
	//	var res []Member
	//	for rows.Next() {
	//		mbr := new(Member)
	//		if err = rows.Scan( &mbr.MID, &mbr.AccountID,&mbr.CustomerID, &mbr.Name,  &mbr.Rating, &mbr.DateAdded ); err != nil {
	//			log.WithFields(logfields).Errorf("could not map rows to their values: %s", err.Error())
	//
	//		} else {
	//			res = append(res, *mbr)
	//			log.WithFields(logfields).Println("returned members : ", res)
	//		}
	//	}
	//	return res

	//}

}

