package round

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"ubanquity.com/libraries/lbsql"
)


func AddRound(db *lbsql.DB, round Round, uuid string) (Round, error) {
	logfields := log.Fields{
		uuid : uuid,
		"Function" : "SQL",
	}

	tx, err := db.Begin()
	if err != nil {
		log.WithFields(logfields).Errorf("transaction failed to start : %s", err.Error())
		return Round{}, errors.New(fmt.Sprintf("transaction failed to start : %s", err.Error()))
	}
	var roundID uint
	err = db.QueryRow(InsertRoundStmt, round.RoundDate, round.RoundNumber, round.GroupID, round.CircleID).Scan(&roundID)
	if err != nil {
		log.WithFields(logfields).Errorf("Failed to insert round : %s", err.Error())
		tx.Rollback()
		return Round{}, errors.New(fmt.Sprintf("Failed to insert round : %s", err.Error()))
	}
	if roundID < 1 {
		log.WithFields(logfields).Errorf("Unable to get the added round's id, so we rollback the transaction ")

		tx.Rollback()

		return Round{}, errors.New("Unable to process The adding of round")
	}
	_ = tx.Commit()
	round.RoundID = roundID
	return round, nil
}
