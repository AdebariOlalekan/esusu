package round

import (
	log "github.com/Sirupsen/logrus"
	"time"
	"ubanquity.com/libraries/lbsql"
)

type Round struct {
	RoundID	uint `json:"round_id"`
	RoundDate time.Time `json:"round_date"`
	RoundNumber uint `json:"round_number"`
	GroupID	uint `json:"group_id"`
	CircleID uint `json:"circle_id"`	
}

func NewRound(roundDate time.Time, roundNumber uint, groupID uint, circleID uint) *Round  {
	return &Round{
		RoundDate:roundDate,
		RoundNumber:roundNumber,
		GroupID:groupID,
		CircleID:circleID,
	}
}

const InsertRoundStmt = `INSERT INTO rounds("round_date", "round_number", "group_id", circle_id) VALUES($1,$2, $3, $4) RETURNING round_id;`
const SelectRoundStmt = `SELECT * FROM rounds WHERE round_id=$1;`
const SelectAllRoundsByGroup = `SELECT * FROM rounds WHERE group_id=$1`
const SelectAllRoundsStmt = `SELECT * FROM rounds`



