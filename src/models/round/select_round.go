package round
import(
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"time"
	"ubanquity.com/libraries/lbsql"
)
func SelectRound(db *lbsql.DB, id uint, uuid string) (Round, error)  {

	logfields := log.Fields{
		"uuid": uuid,
		"function" : "Sql",
	}
	rows, err := db.Query(SelectRoundStmt)
	if err != nil {
		log.WithFields(logfields).Errorf("Unable to Select row: %s", err.Error())
		return Round{}, err
	}

	defer rows.Close()
	var round Round
	for rows.Next() {
		err = rows.Scan(&round.RoundID,&round.RoundDate, &round.RoundNumber, &round.GroupID, &round.CircleID)
		if err != nil{
			log.WithFields(logfields).Errorf("Unable to map rows to struct : %s", err.Error())
			return Round{}, err
		}

	}
	if round.GroupID == 0 && round.RoundNumber == 0 && round.CircleID == 0 {
		log.WithFields(logfields).Errorf("round Not Found with ID : %d", id)
		return Round{}, errors.New(fmt.Sprintf("round Not Found with ID : %d", id))
	}
	return round, nil
}
func SelectRounds(db *lbsql.DB, uuid string, roundID uint, groupID uint) ([]Round, error) {
	logfields := log.Fields{
		"uuid": uuid,
		"function" : "Sql",
	}

	var statement string
	if groupID != 0 {
		statement = SelectAllRoundsByGroup
	}else {
		statement = SelectAllRoundsStmt
	}
	rows , err := db.Query(statement)
	log.WithFields(logfields).Infof("Querying rounds with sql  : %s", statement)
	if err != nil {
		log.WithFields(logfields).Errorf("could not get rounds from database : %s", err.Error())
		return []Round{}, err
	}

	var rounds []Round

	for rows.Next() {
		var round Round
		err = rows.Scan(&round.RoundID,&round.RoundDate, &round.RoundNumber, &round.GroupID, &round.CircleID)
		if err != nil{
			log.WithFields(logfields).Errorf("Could not map rows to struct")
			return []Round{}, err
		}else {
			rounds = append(rounds, round)
			log.WithFields(logfields).Println("returned members : ", rounds)
		}
	}
	return rounds, nil
}
