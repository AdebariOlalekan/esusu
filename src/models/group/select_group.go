package group

import (
	"database/sql"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"ubanquity.com/libraries/lbsql"
)

func SelectGroup(customerID string, uuid string, gid int64) ([]Group, error) {

	logfields := log.Fields{

		"uuid":     uuid,
		"function": "sql",
	}
	var rows *sql.Rows
	var err error
	if gid == 0 {
		log.WithFields(logfields).Infof("Querying database with statement %s:", "SELECT * FROM groups WHERE creator=$1")
		rows, err = SelectGroupByUserStatement.Query(customerID)
	}else {
		log.WithFields(logfields).Infof("Querying database with statement %s:", "SELECT * FROM groups WHERE gid=$1;")
		rows, err = SelectGroupStatement.Query(gid)
	}



	if err != nil {
		log.WithFields(logfields).Errorf("cannot get the required group: %s", err.Error())

		return []Group{}, err
	}

	defer rows.Close()

	var groups  []Group
	for rows.Next() {
		 group := new(Group)
		err := rows.Scan( &group.GID, &group.Name,  &group.ContribAmount, &group.GroupSize,&group.CreationDate,  &group.RoundDuration,  &group.Creator, &group.AccountNumber)
		if  err != nil {
		log.WithFields(logfields).Errorf("can not map rows to their respective values: %s", err.Error())

		return []Group{}, err
		}
			groups = append(groups, *group)

		}
	log.WithFields(logfields).Println("returned groups : ", groups)

	return groups, nil

	}

func SelectAllGroups(db *lbsql.DB, userQuery *FilterQuery, uuid string) []Group {
	logfields := log.Fields{
		"uuid":     uuid,
		"function": "sql",
	}


	var order  = "ASC"
	if !userQuery.Asc {
		order = "DESC"
	}
	if userQuery.SortBy == ""{
		userQuery.SortBy = "name"
	}
	statement := SelectAllGroupsStmt

	if userQuery.GroupSize != 0 {
		statement = fmt.Sprintf(SelectAllGroupByGroupSize, userQuery.GroupSize, userQuery.SortBy, order, userQuery.Offset)
	}
	if userQuery.Rounds != 0 {
		statement = fmt.Sprintf(SelectAllGroupByRounds, userQuery.Rounds, userQuery.SortBy, order, userQuery.Offset)
	}
	if userQuery.ContribAmount != 0 {
		statement = fmt.Sprintf(SelectAllGroupByContribAmount, userQuery.ContribAmount, userQuery.SortBy, order, userQuery.Offset)
	}
	if userQuery.ContribAmount == 0 && userQuery.Rounds == 0 && userQuery.GroupSize == 0 {
		statement = fmt.Sprintf(SelectAllGroupsStmt,userQuery.SortBy, order, userQuery.Offset)
	}



	rows, err := db.Query(statement)
	log.WithFields(logfields).Infof("Getting groups with query :%s ", statement)
	if err != nil {
		log.WithFields(logfields).Errorf("could not get groups from database : %s", err.Error())
		empty := make([]Group, 0)
		return empty
	}

	var res []Group
	for rows.Next() {
		group := new(Group)
		err := rows.Scan( &group.GID, &group.Name,  &group.ContribAmount, &group.GroupSize,&group.CreationDate,  &group.RoundDuration,  &group.Creator, &group.AccountNumber)
		if err != nil {
			log.WithFields(logfields).Errorf("could not map rows to their values: %s", err.Error())

		} else {
			res = append(res, *group)
			log.WithFields(logfields).Println("returned groups : ", res)
		}
	}
	return res

}
