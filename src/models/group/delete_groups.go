package group

import (
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
)

func DeleteGroup(id uint, uuid string, customer_id string) (int64, error){

	logfields := log.Fields{
		"uuid":     uuid,
		"Function": "sql",
	}

	rowsAffected , err := DeleteGroupStmt.Exec(customer_id, id)
	if err != nil {
		log.WithFields(logfields).Errorf("Couldn't Delete group : %s ", err.Error())
		return 0, errors.New(fmt.Sprintf("Couldn't Delete group : %s ", err.Error()))
	}

	affectedRows, err := rowsAffected.RowsAffected()
	if err != nil {
		log.WithFields(logfields).Errorf("Could not count the affected rows : % ", err.Error())
		return 0, errors.New(fmt.Sprintf("Could not count the affected rows : % ", err.Error()))
	}
	log.WithFields(logfields).Infof("The total number of deleted rows : %d", affectedRows)

	return  affectedRows, nil


}
