package group

import (
	log "github.com/Sirupsen/logrus"
	"ubanquity.com/libraries/lbsql"
)

func PatchGroup(id int, group Group, uuid string) bool {

	logfields := log.Fields{
		"uuid" : uuid,
		"Function" : "Sql",
	}
	var name interface{} = group.Name
	var contribAmnt interface{}= group.ContribAmount
	var grpSize interface{}= group.GroupSize
	var roundDuration interface{}= group.RoundDuration
	if group.Name == "" {
		name = nil
	}
	if group.ContribAmount == 0 {
		contribAmnt = nil
	}
	if group.GroupSize == 0 {
		grpSize = nil
	}
	if group.RoundDuration == 0 {
		roundDuration = nil
	}

	row, err := UpdateGroupStmt.Exec(id, name, contribAmnt, grpSize, roundDuration)
	if err != nil {
		log.WithFields(logfields).Errorf("unable to update group : %s", err.Error())
		return false
	}

	rowsAffected, erro := row.RowsAffected()
	if erro != nil {
		log.WithFields(logfields).Errorf("can't count updated rows: %s", err.Error())
		return false
	}

	if rowsAffected == 0 {
		return false
	}

	return true

}

func PatchGroups( db *lbsql.DB, grps []Group, uuid string) bool {
	logfields := log.Fields{
		"uuid" : uuid,
		"Function" : "sql",
	}

	transaction, err := db.Begin()

	if err != nil {
		log.WithFields(logfields).Errorf("cannot start Transaction : %s", err.Error())
		return false
	}

	for _, group := range grps {
		var name interface{} = group.Name
		var contribAmnt interface{}= group.ContribAmount
		var grpSize interface{}= group.GroupSize
		var roundDuration interface{}= group.RoundDuration

		if group.Name == "" {
			name = nil
		}
		if group.ContribAmount == 0 {
			contribAmnt = nil
		}
		if group.GroupSize == 0 {
			grpSize = nil
		}
		if group.RoundDuration == 0 {
			roundDuration = nil
		}
		updatedRws, err := transaction.Stmt(UpdateGroupStmt).Exec(group.GID, name, contribAmnt, grpSize, roundDuration)
		if err != nil {
			log.WithFields(logfields).Errorf("could not update groups from database: %s", err.Error())
			_ = transaction.Rollback()
			return false
		}
		rows, err := updatedRws.RowsAffected()
		if err != nil {
			log.WithFields(logfields).Errorf("Could not count how many rows was updated: %s", err.Error())
			_ = transaction.Rollback()
			return false
		}

		if rows == 0 {
			_ = transaction.Rollback()
			return false
		}
	}
	_ = transaction.Commit()
	return true
}
