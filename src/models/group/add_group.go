package group

import (
	"errors"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"ubanquity.com/libraries/lbsql"
)



func (group Group) AddGroup(db *lbsql.DB,uuid string) (Group, error) {
	logFields := log.Fields{
		"uuid":     uuid,
		"function": "sql",
	}
	// TODO uncomment this below if we dont want a user to create more than one group
	//grp, err := SelectGroup(group.Creator, uuid)
	//if err != nil {
	//	log.WithFields(logFields).Errorf("unable to get the selected group", err.Error())
	//	return Group{}, errors.New("unable to if the group already exist")
	//}
	//if len(grp) >= 1 {
	//	log.WithFields(logFields).Errorf("A group with already exists with that name , please use another")
	//	return group, errors.New("A group with already exists with that name , please use another")
	//}

	tranx, err :=  db.Begin()

	if err != nil {
		log.WithFields(logFields).Errorf("transaction failed to start : %s", err.Error())

		//return empty group
		emptyGroup := Group{}

		return emptyGroup, errors.New(fmt.Sprintf("transaction failed to start : %s", err.Error()))
	}
	var gid int
	err = db.QueryRow(InsertGroupStatement, group.AccountNumber, group.Name, group.ContribAmount, group.GroupSize, group.RoundDuration, group.Creator).Scan(&gid)
	log.WithFields(logFields).Infof("Statment to insert %s", fmt.Sprintf(InsertGroupStatement, group.AccountNumber, group.Name, group.ContribAmount, group.GroupSize, group.RoundDuration, group.Creator))
	if err != nil {
		log.WithFields(logFields).Errorf("Failed to insert group : %s", err.Error())
		tranx.Rollback()
		emptyGroup := Group{}

		return emptyGroup, errors.New(fmt.Sprintf("Failed to insert group : %s", err.Error()))
	}

	if gid < 1 {
		log.WithFields(logFields).Errorf("Unable to get the added group's id, so we rollback the transaction ")

		tranx.Rollback()
		emptyGroup := Group{}
		return emptyGroup, errors.New("Unable to process The adding of group")
	}
	tranx.Commit()
	group.GID = uint(gid)
	return group, nil
}
