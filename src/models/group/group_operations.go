package group

import (
	"esusu/src/models/member"
	"esusu/src/service_calls"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"ubanquity.com/libraries/lbsql"
)

// AddMember : this function adds a member to a group
func (group *Group) AddMember(Db *lbsql.DB, nmember member.Member, uuid string)(map[string]interface{}, error) {
	logfields := log.Fields{
		"uuid": uuid,
		"Handler": "Add member to Group",
	}


	accounts, err := service_calls.GetUserAccount(nmember.AccountID, uuid)
	if err != nil {
		log.WithFields(logfields).Errorf("Error While validating member account : %s", err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : err.Error(),
		}
		return resp, err
	}

	if accounts["balance_available"].(float64) <= 0 {
		log.WithFields(logfields).Errorf("Customer have insufficient balance on the selected account. making it ineligible: %s", err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : "Customer have insufficient balance on the selected account. making it ineligible",
		}
		return resp, errors.New("Customer have insufficient balance on the selected account. making it ineligible")
	}
	//grp, err := SelectGroup(nmember.CustomerID, uuid)
	//if err != nil {
	//	log.WithFields(logfields).Errorf("Unable to verify group prior existence: %s", err.Error())
	//	resp := map[string]interface{}{
	//		"status" : "failed",
	//		"message" : fmt.Sprintf("Unable to verify group prior existence: %s", err.Error()),
	//	}
	//	return resp, errors.New("Unable to verify group prior existence : " + err.Error())
	//
	//}
	//var resp map[string]interface{}
	//if len(grp) < 1 {
	//	mem, err := member.AddMember(Db, nmember, uuid)
	//	if err != nil {
	//		log.WithFields(logfields).Errorf(" %s", err.Error())
	//		resp := map[string]interface{}{
	//			"status" : "failed",
	//			"message" : fmt.Sprintf("Member Account cannot be created : %s", err.Error()) ,
	//		}
	//		return resp, err
	//	}
	//	resp := map[string]interface{}{
	//		"name" : mem.Name,
	//		"customer_id": mem.CustomerID,
	//		"Group Name" : group.Name,
	//		"rounds" : group.RoundDuration,
	//		"group size" : group.GroupSize,
	//	}
	//	return resp, nil
	mem, err := nmember.AddMember(Db, uuid)
	if err != nil {
		log.WithFields(logfields).Errorf(" %s", err.Error())
		resp := map[string]interface{}{
			"status" : "failed",
			"message" : fmt.Sprintf("Member Account cannot be created : %s", err.Error()) ,
		}
		return resp, err
	}
	resp := map[string]interface{}{
		"name" : mem.Name,
		"customer_id": mem.CustomerID,
		"Group Name" : group.Name,
		"rounds" : group.RoundDuration,
		"group size" : group.GroupSize,
	}
	// TODO we should send notification to the user here prompting them of the member creation
	return resp, nil
}
