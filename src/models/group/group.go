package group

import (
	log "github.com/Sirupsen/logrus"
	"time"
	"ubanquity.com/libraries/lbsql"
)

type money int64

type Group struct {
	GID           uint      `json:"gid"`
	Name          string    `json:"name"`
	ContribAmount money     `json:"contrib_amount"`
	GroupSize     int       `json:"group_size"`
	CreationDate  time.Time `json:"creation_date"`
	RoundDuration int       `json:"round_duration"`
	Creator	      string 	`json:"creator"`
	AccountNumber string    `json:"account_number"`
}


type FilterQuery struct {
	Offset        int64  `json:"offset"`
	Limit         int64  `json:"limit"`
	SortBy        string `json:"sortby"`
	Asc           bool   `json:"asc"`
	Name          string `json:"name"`
	GroupSize     int64  `json:"group_size"`
	ContribAmount int64  `json:"contrib_amount"`
	Rounds		  int64 `json:"rounds"`
}

var InsertGroupStatement string
var SelectGroupStatement *lbsql.Stmt
var SelectGroupByUserStatement *lbsql.Stmt
var SelectAllGroupsStmt string
var SelectAllGroupByContribAmount string
var SelectAllGroupByGroupSize string
var SelectAllGroupByRounds string

var DeleteGroupStmt *lbsql.Stmt
var UpdateGroupStmt *lbsql.Stmt

 func PreparedStatements(db *lbsql.DB) {
	log.Infof("preparing sql statements for Groups")
	var err error

	InsertGroupStatement = `INSERT INTO groups("account_number","name","contribution_amount", "group_size", "round_duration", "creator") VALUES ($1,$2, $3, $4, $5, $6) RETURNING gid;`

	SelectGroupStatement, err = db.Prepare(`SELECT gid, name, contribution_amount, group_size, creation_date::date, round_duration, creator, account_number FROM groups WHERE gid=$1;`)
	if err != nil {
		log.Errorf("can't prepare select group statement: %s", err.Error())
	}
	SelectGroupByUserStatement, err = db.Prepare(`SELECT * FROM groups WHERE creator like $1;`)
	 if err != nil {
		 log.Errorf("can't prepare select group statement: %s", err.Error())
	 }

	SelectAllGroupsStmt = `SELECT *  FROM groups ORDER BY %s %s  OFFSET %d`
	SelectAllGroupByContribAmount = `SELECT * FROM groups WHERE contribution_amount=$d ORDER BY %s %s LIMIT %d OFFSET %d`
	SelectAllGroupByRounds = `SELECT * FROM groups WHERE round_duration=$d ORDER BY %s %s  OFFSET %d`
	SelectAllGroupByGroupSize = `SELECT * FROM groups WHERE group_size=%d ORDER BY %s %s  OFFSET %d`

	 DeleteGroupStmt, err = db.Prepare(`DELETE FROM groups WHERE creator like $1 AND gid=$2;`)
	if err != nil {
		log.Errorf("can't prepare delete group statement: %s", err.Error())
	}

	UpdateGroupStmt, err = db.Prepare(`UPDATE groups SET name=coalesce($2, name), contribution_amount=coalesce($3, contribution_amount), group_size=coalesce($4, group_size), round_duration=coalesce($5, round_duration) WHERE gid=$1;`)
	if err != nil {
		log.Errorf("can't prepare update group statement: %s", err.Error())
	}

}

func CloseStatements() {
	log.Infof("closing sql statements")
	SelectGroupStatement.Close()
	DeleteGroupStmt.Close()
	UpdateGroupStmt.Close()
}
