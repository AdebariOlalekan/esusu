package main

import (
	"esusu/src/misc"
	"esusu/src/models/group"
	log "github.com/Sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
)

func GetGroup( w http.ResponseWriter, r *http.Request)  {

	uuid := r.Header.Get("X-Correlation-ID")

	logfields := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Select Group",
	}

	gid , err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
	if err != nil {
		log.WithFields(logfields).Infof("No Group id specified, \n getting the logged in user groups")
	}

	if err := r.Body.Close(); err != nil {
		log.WithFields(logfields).Errorf("The Request Couldn't be closed")
		misc.Response(w, "", logfields, http.StatusInternalServerError)
		return
	}
	userID := strings.Split(r.Context().Value("user").(string), "/")[0]
	groups, err := group.SelectGroup(userID, uuid, gid)

	if err != nil {
		response := "An error occured: " + err.Error()
		misc.Response(w, response, logfields, http.StatusInternalServerError)
		return
	} else {
		log.WithFields(logfields).Infof("Get group - Success, %s", groups)
		if len(groups) < 1	&& gid != 0 {
			msg := "Groups with the specified id Not Found"
			misc.Response(w, msg, logfields, http.StatusBadRequest)
			return
		}else if len(groups) < 1 && gid == 0 {
			msg := "This user has not created any group"
			misc.Response(w, msg, logfields, http.StatusBadRequest)
			return
		}
		misc.Response(w, groups, logfields, http.StatusOK)
		return
	}

}

func GetGroups(w http.ResponseWriter, r *http.Request)  {

	uuid := r.Header.Get("X-Correlation-ID")

	logfields := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Select Group",
	}

	if err  := r.Body.Close(); err != nil {
		log.WithFields(logfields).Infof("Cannot close request body : %s", err)
		misc.Response(w, "", logfields, http.StatusInternalServerError)
		return
	}

	filter := new(group.FilterQuery)

	var errLimit, errOfset, errAsc, errGrpSize, errContribAmt, errRounds error

	query := r.URL.Query()

	filter.GroupSize, errGrpSize = strconv.ParseInt(query.Get("group_size"), 10, 64)
	filter.ContribAmount, errContribAmt = strconv.ParseInt(query.Get("contrib_amount"), 10, 64)
	filter.Rounds, errRounds = strconv.ParseInt(query.Get("rounds"), 10, 64)
	filter.Limit, errLimit = strconv.ParseInt(query.Get("limit"), 10, 64)
	filter.Offset, errOfset = strconv.ParseInt(query.Get("offset"), 10, 64)
	filter.Asc, errAsc = strconv.ParseBool(query.Get("asc"))
	filter.SortBy = query.Get("sortby")

	log.WithFields(logfields).Infof("Get Accounts - with filters : OffSet:%+v sortby:%+v Asc:%+v limit:%+v name:%+v  group_size:%+v contribution_amount:%+v ", filter.Offset, filter.SortBy, filter.Asc, filter.Limit, filter.Name, filter.GroupSize, filter.ContribAmount)

	if errLimit != nil || errOfset != nil || errAsc != nil || errGrpSize != nil || errContribAmt != nil || errRounds != nil {
		if errOfset != nil {
			log.WithFields(logfields).Errorf("wrong query string offset: %+v", errOfset.Error())
		}

		if errLimit != nil {
			log.WithFields(logfields).Errorf("wrong query string limit: %+v", errLimit.Error())
		}

		if errAsc != nil {
			log.WithFields(logfields).Errorf("wrong query string asc: %+v", errAsc.Error())
		}

		if filter.SortBy == "" {
			log.WithFields(logfields).Errorf("wrong query string sortby")
		}
		if filter.Rounds == 0 {
			log.WithFields(logfields).Errorf("wrong query string sortby")
		}
	}

	groups := group.SelectAllGroups(Db, filter, uuid)

	if len(groups) <= 0 {
		log.WithFields(logfields).Infof("get groups - no users found matching the query")
		responseBody := "No Groups found matching the query"
		misc.Response(w,responseBody,logfields, http.StatusBadRequest)
		return
	}
		log.WithFields(logfields).Infof("get groups - success")
		misc.Response(w,groups,logfields,http.StatusOK)
	}

