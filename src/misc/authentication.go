package misc

import (
	"context"
	log "github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes/timestamp"
	"io/ioutil"
	"net/http"
	"strings"
)

type token struct {
	User string
	Type string
	exp timestamp.Timestamp
	jwt.StandardClaims
}
var publicKey []byte
func init(){
	publicKey, _  = ioutil.ReadFile("/data/ubanquity/conf/keys/publickey.rsa.pub")
}

var Authentication = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uuid := r.Header.Get("X-Correlation-Id")
		logfields := log.Fields{
			"uuid" : uuid ,
			"Handler" : "Select Member",
	}

		tokenHeader := r.Header.Get("Authorization")
		if tokenHeader == "" {
			Response(w, "You re not Authenticated to go further, kindly provide a token", logfields, 403)
			return
		}
		splitted := strings.Split(tokenHeader, " ")
		if len(splitted) != 2 {
			Response(w, "Invalid/Malformed auth token", logfields, 403)
			return
		}
		tk := &token{}
		token := splitted[1]

		signingKey, _ := jwt.ParseRSAPublicKeyFromPEM(publicKey)
		tok , err := jwt.ParseWithClaims(token, tk, func(token *jwt.Token)(interface{}, error){
			return signingKey, nil
		})
		if err != nil {
			Response(w, "Unable to sign or verify token signature", logfields, 403)
			log.WithFields(logfields).Errorf("token failure - view stacktrace below : %s", err.Error())
			return
		}
		if !tok.Valid {
			Response(w, "Authentication Token is not valid.", logfields, 403)
			log.WithFields(logfields).Errorf("Authentication Token is not valid.")

			return
		}
		if tk.Type != "customer" {
			Response(w, "Authentication Token is not valid; this service should be accessed with a customer token", logfields, 403)
			log.WithFields(logfields).Errorf("Authentication Token is not a customer token. %s", tk.Type)
			return
		}
		if tk.User == "" {
			Response(w, "Authentication Token is not valid as it doesnt contains a user", logfields, 403)
			log.WithFields(logfields).Errorf("Authentication Token doesnt contain a user. %s", tk.Type)
			return
		}
		ctx := context.WithValue(r.Context(), "user", tk.User )
		log.WithFields(logfields).Infof("The current http context includes the user :%s ", ctx.Value("user"))
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})


}
