package misc

import (
	"fmt"
	log "github.com/Sirupsen/logrus"
	"net/http"
	"ubanquity.com/libraries/util"
)

func Response(w http.ResponseWriter, message interface{}, logfields map[string]interface{}, status int) {
	// TODO remove json.Marshal and consider using a json encoder that can encode witha struct like json.NewEncoder().Encode()
	data, err := util.JsonMarshal(message, false)
	if err != nil {
		log.WithFields(logfields).Errorf("Could not jsonify the message")
		w.Header().Set("Content-type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(status)
		_,_ = fmt.Fprintf(w, "Could not jsonify the message")
	}
	log.WithFields(logfields).Info("returned data : ", message)
	w.Header().Set("Content-type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)
	w.Write(data)
}
