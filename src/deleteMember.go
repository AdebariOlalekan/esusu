package main

import (
	"esusu/src/misc"
	"esusu/src/models/member"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func DeleteMember(w http.ResponseWriter, r *http.Request)   {

	uuid := w.Header().Get("X-CorrelationID")

	logfields := log.Fields{
		"uuid" : uuid,
		"Handler" : "Delete Member",
	}
	if err := r.Body.Close(); err != nil {
		log.WithFields(logfields).Errorf("can't close request: %s", err.Error())
		misc.Response(w,"", logfields, http.StatusInternalServerError)
		return
	}
	mid, err := strconv.ParseUint(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		log.WithFields(logfields).Errorf("invalid member id: %s", err.Error())
		misc.Response(w, "The inputed Member id is invalid", logfields, http.StatusBadRequest)
		return
	}
	rw := member.DeleteMember(mid, uuid)
	if rw <= 0 {
		log.WithFields(logfields).Errorf("could not delete Member")
		misc.Response(w,"",logfields, http.StatusInternalServerError)
	}
	log.WithFields(logfields).Infof("Member deleted : %d", rw)
	misc.Response(w, "Member successfully deleted", logfields, http.StatusOK)

}
