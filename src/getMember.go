package main

import (
	"esusu/src/misc"
	"esusu/src/models/member"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func GetMember( w http.ResponseWriter, r *http.Request)  {

	uuid := r.Header.Get("X-Correlation-ID")
	logfields := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Select Member",
	}

	if err := r.Body.Close(); err != nil {
		log.WithFields(logfields).Errorf("The Request Couldn't be closed")
		misc.Response(w, "", logfields, http.StatusInternalServerError)
		return
	}

	mid, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)

	if err != nil {
		log.WithFields(logfields).Errorf("Invalid path parametre : %s", err.Error())
		misc.Response(w, "The path paramtre passed in is invalide, kindly try a numeric value", logfields, http.StatusBadRequest)
		return

	}

	mbr, err := member.SelectMember(uint64(mid), uuid)

	if err != nil {
		response := fmt.Sprintf("Cannot get Member:  %s",err.Error())
		misc.Response(w, response, logfields, http.StatusNotFound)
		return

	}
	log.WithFields(logfields).Infof("Get account - Success, %s", mbr)
	misc.Response(w, mbr, logfields, http.StatusOK)
	return

}

func GetMembers(w http.ResponseWriter, r *http.Request)  {

	uuid := r.Header.Get("X-Correlation-ID")

	logfields := log.Fields{
		"uuid" : uuid ,
		"Handler" : "Select Member",
	}

	if err  := r.Body.Close(); err != nil {
		log.WithFields(logfields).Infof("Cannot close request body : %s", err)
		misc.Response(w, "", logfields, http.StatusInternalServerError)
		return
	}

	filter := new(member.FilterQuery)

	var  errOfset, errGid,  errAsc, errRating error

	query := r.URL.Query()

	filter.Rating, errRating = strconv.ParseFloat(query.Get("rating"),  64)
	//filter.DateAdded, errDateAdded = strconv.ParseInt(query.Get("date"), 10, 64)

	filter.Offset, errOfset = strconv.ParseInt(query.Get("offset"), 10, 64)
	filter.Asc, errAsc = strconv.ParseBool(query.Get("asc"))
	filter.SortBy = query.Get("sortby")
	filter.GroupID, errGid = strconv.ParseInt(query.Get("group_id"), 10, 64)

	log.WithFields(logfields).Infof("Get Members - with filters : OffSet:%+v sortby:%+v Asc:%+v  rating:%+v ", filter.Offset, filter.SortBy, filter.Asc, filter.Limit, filter.Rating)

	if errOfset != nil || errAsc != nil || errRating != nil {
		if errOfset != nil {
			log.WithFields(logfields).Errorf("wrong query string offset: %+v", errOfset.Error())
		}
		if errAsc != nil {
			log.WithFields(logfields).Errorf("wrong query string asc: %+v", errAsc.Error())
		}

		if filter.SortBy == "" {
			log.WithFields(logfields).Errorf("wrong query string sortby")
		}
		if errGid != nil {
			log.WithFields(logfields).Errorf("Wrong query integer for group id %+v", errGid.Error())
		}


	}

	members := member.SelectAllMembers(Db ,filter, uuid)

	if len(members) <= 0 {
		log.WithFields(logfields).Infof("get members - no member found matching the query")
		responseBody := "No users found matching the query"
		misc.Response(w,responseBody,logfields, http.StatusBadRequest)
		return
	}
	log.WithFields(logfields).Infof("get members - success")
	misc.Response(w,members,logfields,http.StatusOK)
}

