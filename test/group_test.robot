*** Settings ***
Documentation    Suite description
Library                             Collections
Library                             RequestsLibrary

*** Variables ***
${URL}                              http://localhost:10000/esusu
${CUSTOMER TOKEN USER 1}            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE4ODA4OTI5MjIsIlVzZXIiOiI2NDIzNTM3L2NqNjQyMzUzNyIsIlR5cGUiOiJjdXN0b21lciIsIkRldmljZV9pZCI6Mzc3LCJBcHBfaWQiOiIiLCJBX1VzZXJfaWQiOm51bGx9.CUiMxObNqaKd97-A7SEC6CcWjoGZ6ISt7ILjBd3xhJT6sfZ3YwrwhqfnzmNPo0Vz-xskjRAlDi_kd2UPY1I_bjuLFXi4V3lohxtTyuTL7Mra5xqLOvoi8cROA-e0IJqw3VY5NJeKqTrxnyRYRsgTuyz0suAKq1kZyJwT5ULi6TTN3eZcc48Aa3GMOzAihMXkD2b9k6nijl8xiyQE4qE1RjIhD7HdfEyIQoWJ7bifACpXm6q-DYMd0haRm7c35CQ9Ha9ng-oEg8P8QJPSaRj2XOripyHzKASqT55rm8x7maAmnuJlLSDcKg1wVdvOYpV0UcfkVd9BxtjQm_WEHL1tHJJYUlALovciyp4AApnqNIautBVHclC-4Hm3pvh8Lp3eVFwG2oODZK69wmJ-ROcCzlPF6ZLfAqyIY2CfaUYARu5uKg0ssB-qceRnQyNJkaXEhlmitVWGw1i9Lj80B45sF66ol9G1--rGNVoS990YKbANzRnLH9FHMfe-VU99UMWhagmeB4JDwhBNRV5F7oY1dI8LOQTKEphSboTk7ZSfTX3XmnJg_hFJEfu3nqAlMcqCeNQ2o7G7V_yV0oPapQyIS9eeyn4VUEaoMUlmUouYpryVOLA5NHdw7XRNOoT7O1O-FRNUywCNUvGt6iUnjjkxWVH6VZMDZQfrD26Y_Rn0TQ
${CUSTOMER TOKEN USER 2}            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE4ODA5ODQ2MTcsIlVzZXIiOiIzMjg1NzQvam9obm55IiwiVHlwZSI6ImN1c3RvbWVyIiwiRGV2aWNlX2lkIjoyNDUsIkFwcF9pZCI6IiIsIkFfVXNlcl9pZCI6bnVsbH0.BU2N6tBsMEEWqgaMoVPeoDqH1yTF70_6S1pHWntd1hL_LYJ6y9W8I2gOdZzDKbxK68hT8eLIcDmFnWxVgOa9RpSHYoRJT0LLCT40XMZfFP9NG-kjT3BwDePWdzKyW4Zh4g0u2hKsw_i-B64w6xIpdVvrZ1peZa7w-rjUJL4RfvMW3gCEUYeY-iVr6xtzzVTrtSul8n8mGyjEKHO3EeR6U-MI5d7wPE5GskgfiP5jEKHAWnFBk4-f8ZS1UMgyZR2qbgmmpuWoIJAHgXrVp1JEBY7yfdOnVQ211mYCMTEbfFCDex7UOMcdtPMdBq7Psd6r9Ja9-2GTDYNZk22PsMDKRWyobjOdzSrEjkVUmOX3ryzZiA7DqfjEq_5dB2IFKXz2HKwLQBBxuCm_1Hp7f_yNYRf_-yMcl3iXb5R1qX7PCoknJh0T-1MBPRS_ZyuPYQvQTg218ProCPQPAPdt0kynyA8AbBvhK9cCM7dZi7T35pUkv3a96Ieanq5DlMDhlLz_mNxBkgV5tg770fnYKhk81jqnwb12BVulez7jq7nWI86GXelBs9LQPKqRvpd3cq01BnkF5dTNCc9AQA9F87fh17dj-BHarFMaJYqDMiDcH_OlQQzPCcbRl99QB--m7S8rZyu2rv9B0abKySuLzIrApcDwylnqX7wxJbZAsmPsCw
${SERVER ERROR}                     500
${BAD REQUEST}                      400
${OK}                               200
${CREATED}                          201
${GROUP NOT FOUND}                  "Groups with the specified id Not Found"

&{HEADERS DEFAULT}                  Content-Type=application/json                Authorization=${CUSTOMER TOKEN USER 1}
&{HEADERS DEFAULT 2}                Content-Type=application/json                Authorization=${CUSTOMER TOKEN USER 2}
${HEADER URL ENCODED}               Content-Type=application/x-www-form-urlencoded
${HEADER JSON}                      Content-Type=application/json

${NON EXISTING USER ID}             0
${NON EXISTING USER ERROR}          User not found
${UPDATE FAILED}                    Update failed
${NO ACCOUNT NUMBER ERROR}          "You must enter an Account Number which must be equal to 10 digits"

&{OGUN127}                           name=OG78292     contrib_amount=2000     group_size=21       round_duration=4    account_number=2020533676
${OGUN127 INCOMPLETE JSON}          {"name":"&{OGUN127}[name]",	"contrib_amount": &{OGUN127}[contrib_amount],"group_size": &{OGUN127}[group_size]}
${OGUN127 JSON}                      {"name":"&{OGUN127}[name]","contrib_amount": &{OGUN127}[contrib_amount],"group_size": &{OGUN127}[group_size],"round_duration" : &{OGUN127}[round_duration],"account_number" : "&{OGUN127}[account_number]"}
${LAG009 USER}                     {"name": "LAG009","contrib_amount": 3000,"group_size": 5,"round_duration": 4,"account_number" : "1020047624"}
${LAG009 USER INCOMPLETE}           {"contrib_amount": 5000,"group_size": 13,"round_duration": 6,"account_number" : "1020047624"}
${LAG009 INCOMPLETE RESPONSE}       {"Membership details": {"Group Name": "LAG009","customer_id": "328574","group size": 5,"name": "JOHN UBAH","rounds": 4},"group details": {"gid": 14,"name": "LAG009","contrib_amount": 3000,"group_size": 5,"creation_date": "0001-01-01T00:00:00Z","round_duration": 4,"creator": "328574","account_number": "1020047624"}}
${LAG009 RESPONSE}                  {"Membership details": {"Group Name": "LAG009","customer_id": "328574","group size": 5,"name": "JOHN UBAH","rounds": 4},"group details": {"gid": 14,"name": "LAG009","contrib_amount": 3000,"group_size": 5,"creation_date": "0001-01-01T00:00:00Z","round_duration": 4,"creator": "328574","account_number": "1020047624"}}
${GROUP ID}                          0



*** Test Cases ***
#Post Group Should Fail
#    Create Session          get_groups          ${URL}
##   REQUEST HEADERS         REQUEST BODY                        STATUS CODE         RESPONSE BODY
##    ${HEADER JSON}         ${OGUN127 INCOMPLETE JSON}           ${BAD REQUEST}      ${NO ACCOUNT NUMBER ERROR}
#    ${response}=            get request         get_groups       /groups            headers=&{HEADERS DEFAULT}
#    log to console          ${response.status_code}
#    log to console          ${response.content}

Post Group Should Fail
    [Template]  Post Group Should Fail
    #   ${DATA}             ${HEADERS}          ${RESPONSE STATUS}          ${EXPECTED BODY}
    ${OGUN127 INCOMPLETE JSON}         ${HEADERS DEFAULT}           ${BAD REQUEST}          ${NO ACCOUNT NUMBER ERROR}

Post Group Should Pass
    [Template]  Post Group Should Pass
#   ${DATA}             ${HEADERS}          ${RESPONSE STATUS}          ${EXPECTED BODY}
    ${LAG009 USER}                  ${HEADERS DEFAULT 2}            ${CREATED}           ${LAG009 RESPONSE}

#   --------------------------------
Get Groups Should Pass
    [Template]  Get Groups Should Pass
#    OFFSET             SORTBY          ASC         STATUS CODE         GROUP SIZE          CONTRIB AMOUNT          ROUNDS
    ${EMPTY}            ${EMPTY}        ${EMPTY}    ${OK}               0                       0                       0

Get Group Should Pass
    [Template]      Get Group Should Pass
#    12              ${OK}               1
    ${EMPTY}        ${OK}               0
    ${GROUP ID}   ${OK}                 1


Get Group Should Fail[gid]
    [Template]  Get Group Should Fail
    400                  ${BAD REQUEST}          ${GROUP NOT FOUND}

#   ----------------------------


Delete Group Should pass
    [Template]  Delete Group Should pass
#    ${ID}                   ${HEADERS}                ${RESPONSE STATUS}            ${EXPECTED RESPONSE}
     ${GROUP ID}                  ${HEADERS DEFAULT 2}       ${OK}                           "Affected Groups : 1"

Delete Group Should Fail
    [Template]  Delete Group Should Fail
    #    ${ID}                   ${HEADERS}                ${RESPONSE STATUS}            ${EXPECTED RESPONSE}
        300                     ${HEADERS DEFAULT 2}        ${OK}                           "Affected Groups : 0"

*** Keywords ***

Get Groups Should Pass
    [Arguments]     ${OFFSET}       ${SORTBY}       ${ASC}      ${RESPONSE STATUS}      ${GROUP SIZE}       ${CONTRIB AMOUNT}       ${ROUNDS}
    Get Groups      ${OFFSET}       ${SORTBY}       ${ASC}      ${RESPONSE STATUS}      ${GROUP SIZE}       ${CONTRIB AMOUNT}       ${ROUNDS}
    Should Be Equal As Strings      ${STATUS}       ${RESPONSE STATUS}
    ${LENGTH}=      Get Length      ${BODY.json()}

Get Groups
    [Arguments]         ${OFFSET}           ${SORTBY}           ${ASC}      ${RESPONSE STATUS}      ${GROUP SIZE}       ${CONTRIB AMOUNT}       ${ROUNDS}
    Create Session      esusu               ${URL}
    ${RESPONSE}=        get request         esusu               /groups?offset=${OFFSET}&sortby=${SORTBY}&asc=${ASC}&group_size=${GROUP SIZE}&contrib_amount=${CONTRIB AMOUNT}&rounds=${ROUNDS}         headers=&{HEADERS DEFAULT}
#    log to console      ${RESPONSE}
    Set Test Variable   ${STATUS}           ${RESPONSE.status_code}
    Set Test Variable   ${BODY}             ${RESPONSE}

#-----------------
Get Group Should Pass
    [Arguments]                 ${ID}           ${RESPONSE STATUS}          ${GROUPS LENGTH}
    Get Group                   ${ID}
    Should Be Equal As Strings   ${STATUS}       ${RESPONSE STATUS}
    ${LENGTH}=                  Get Length       ${BODY.json()}
#    Should Be Equal As Strings   ${LENGTH}        ${GROUPS LENGTH}

Get Group Should Fail
    [Arguments]  ${ID}           ${RESPONSE STATUS}     ${EXPECTED BODY}
    Get Group                   ${ID}
    Should Be Equal As Strings   ${STATUS}              ${RESPONSE STATUS}
    Should Be Equal As Strings   ${BODY.content}        ${EXPECTED BODY}

Get Group
    [Arguments]                 ${ID}
    Create Session              esusu               ${URL}
    ${RESPONSE}=                get request         esusu               /group?id=${ID}             headers=&{HEADERS DEFAULT 2}
    Set Test Variable           ${STATUS}           ${RESPONSE.status_code}
    Set Test Variable           ${BODY}             ${RESPONSE}

# ------------------------

Post Group Should Fail
    [Arguments]  ${DATA}             ${HEADERS}        ${RESPONSE STATUS}          ${EXPECTED BODY}
    Post Group      ${DATA}           ${HEADERS}
    Should Be True      ${STATUS}==${RESPONSE STATUS}
    Should Be Equal As Strings      ${BODY.content}         ${EXPECTED BODY}

Post Group Should Pass
    [Arguments]         ${DATA}            ${HEADERS}          ${RESPONSE STATUS}          ${EXPECTED BODY}
    Post Group          ${DATA}           ${HEADERS}
    Should Be True      ${STATUS}==${RESPONSE STATUS}
    ${EXPECTED}=        To Json           ${EXPECTED BODY}
    Should Be Equal As Strings      ${BODY.json()}[group details][name]         ${EXPECTED}[group details][name]
    Run Keyword If      ${BODY.json()}[group details][gid] > 0        Set Suite Variable      ${GROUP ID}         ${BODY.json()}[group details][gid]

Post Group
    [Arguments]         ${DATA}             ${HEADERS}
    Create Session      esusu               ${URL}
    ${RESPONSE}=        Post Request        esusu               /groups/create/             data=${DATA}                 headers=${HEADERS}
    Set Test Variable   ${STATUS}           ${RESPONSE.status_code}
    Set Test Variable   ${BODY}         ${RESPONSE}

#   --------------------------------

Delete Group Should pass
    [Arguments]             ${ID}                   ${HEADERS}                ${RESPONSE STATUS}            ${EXPECTED RESPONSE}
    Delete Group            ${ID}                   ${HEADERS}
    Should Be True          ${STATUS}==${RESPONSE STATUS}
    Should Be Equal As Strings  ${BODY.content}             ${EXPECTED RESPONSE}

Delete Group Should Fail
    [Arguments]             ${ID}                   ${HEADERS}                ${RESPONSE STATUS}            ${EXPECTED RESPONSE}
    Delete Group            ${ID}                   ${HEADERS}
    Should Be True          ${STATUS}==${RESPONSE STATUS}
    Should Be Equal As Strings   ${BODY.content}         ${EXPECTED RESPONSE}

Delete Group
    [Arguments]             ${ID}                   ${HEADERS}
    Create Session          esusu                   ${URL}
    ${RESPONSE}=            Delete Request          esusu                   /groups/${ID}/remove/                   headers=${HEADERS}
    Set Test Variable       ${STATUS}               ${RESPONSE.status_code}
    Set Test Variable       ${BODY}                 ${RESPONSE}

