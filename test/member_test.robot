*** Settings ***
Documentation    Suite description
Library             Collections
Library             RequestsLibrary

*** Variables ***
*** Variables ***
${URL}                              http://localhost:10000/esusu
${CUSTOMER TOKEN USER 1}            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE4ODA4OTI5MjIsIlVzZXIiOiI2NDIzNTM3L2NqNjQyMzUzNyIsIlR5cGUiOiJjdXN0b21lciIsIkRldmljZV9pZCI6Mzc3LCJBcHBfaWQiOiIiLCJBX1VzZXJfaWQiOm51bGx9.CUiMxObNqaKd97-A7SEC6CcWjoGZ6ISt7ILjBd3xhJT6sfZ3YwrwhqfnzmNPo0Vz-xskjRAlDi_kd2UPY1I_bjuLFXi4V3lohxtTyuTL7Mra5xqLOvoi8cROA-e0IJqw3VY5NJeKqTrxnyRYRsgTuyz0suAKq1kZyJwT5ULi6TTN3eZcc48Aa3GMOzAihMXkD2b9k6nijl8xiyQE4qE1RjIhD7HdfEyIQoWJ7bifACpXm6q-DYMd0haRm7c35CQ9Ha9ng-oEg8P8QJPSaRj2XOripyHzKASqT55rm8x7maAmnuJlLSDcKg1wVdvOYpV0UcfkVd9BxtjQm_WEHL1tHJJYUlALovciyp4AApnqNIautBVHclC-4Hm3pvh8Lp3eVFwG2oODZK69wmJ-ROcCzlPF6ZLfAqyIY2CfaUYARu5uKg0ssB-qceRnQyNJkaXEhlmitVWGw1i9Lj80B45sF66ol9G1--rGNVoS990YKbANzRnLH9FHMfe-VU99UMWhagmeB4JDwhBNRV5F7oY1dI8LOQTKEphSboTk7ZSfTX3XmnJg_hFJEfu3nqAlMcqCeNQ2o7G7V_yV0oPapQyIS9eeyn4VUEaoMUlmUouYpryVOLA5NHdw7XRNOoT7O1O-FRNUywCNUvGt6iUnjjkxWVH6VZMDZQfrD26Y_Rn0TQ
${CUSTOMER TOKEN USER 2}            Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE4ODA5ODQ2MTcsIlVzZXIiOiIzMjg1NzQvam9obm55IiwiVHlwZSI6ImN1c3RvbWVyIiwiRGV2aWNlX2lkIjoyNDUsIkFwcF9pZCI6IiIsIkFfVXNlcl9pZCI6bnVsbH0.BU2N6tBsMEEWqgaMoVPeoDqH1yTF70_6S1pHWntd1hL_LYJ6y9W8I2gOdZzDKbxK68hT8eLIcDmFnWxVgOa9RpSHYoRJT0LLCT40XMZfFP9NG-kjT3BwDePWdzKyW4Zh4g0u2hKsw_i-B64w6xIpdVvrZ1peZa7w-rjUJL4RfvMW3gCEUYeY-iVr6xtzzVTrtSul8n8mGyjEKHO3EeR6U-MI5d7wPE5GskgfiP5jEKHAWnFBk4-f8ZS1UMgyZR2qbgmmpuWoIJAHgXrVp1JEBY7yfdOnVQ211mYCMTEbfFCDex7UOMcdtPMdBq7Psd6r9Ja9-2GTDYNZk22PsMDKRWyobjOdzSrEjkVUmOX3ryzZiA7DqfjEq_5dB2IFKXz2HKwLQBBxuCm_1Hp7f_yNYRf_-yMcl3iXb5R1qX7PCoknJh0T-1MBPRS_ZyuPYQvQTg218ProCPQPAPdt0kynyA8AbBvhK9cCM7dZi7T35pUkv3a96Ieanq5DlMDhlLz_mNxBkgV5tg770fnYKhk81jqnwb12BVulez7jq7nWI86GXelBs9LQPKqRvpd3cq01BnkF5dTNCc9AQA9F87fh17dj-BHarFMaJYqDMiDcH_OlQQzPCcbRl99QB--m7S8rZyu2rv9B0abKySuLzIrApcDwylnqX7wxJbZAsmPsCw
${SERVER ERROR}                     500
${BAD REQUEST}                      400
${OK}                               200
${CREATED}                          201
${NOT FOUND}                        404

&{HEADERS DEFAULT}                  Content-Type=application/json                Authorization=${CUSTOMER TOKEN USER 1}
&{HEADERS DEFAULT 2}                Content-Type=application/json                Authorization=${CUSTOMER TOKEN USER 2}

${MEMBER ID}                        0
${MEMBER ACCOUNT ID}                ""
&{AKIN ADEYEMI}                     account_id="2000426941"                       group_id=43
${AKIN ADEYEMI JSON}                {"account_id":&{AKIN ADEYEMI}[account_id],"group_id" : &{AKIN ADEYEMI}[group_id]}
&{AKIN ADEYEMI INCOMPLETE}          account_id=2000426941
${INCOMPLETE DATA ERROR}            Incomplete Member registeration data please review. : Account_id and group_id are compulsary
${MEMBER NOT FOUND ERROR}           Cannot get Member:  member Not Found with ID : 400
${INVALID PATH PARAMETRE}           The path paramtre passed in is invalide, kindly try a numeric value

*** Test Cases ***
Post Member Should Fail
    [Template]  Post Member Should Fail
#   ${DATA}                     ${RESPONSE STATUS}                  ${EXPECTED RESPONSE}
    ${AKIN ADEYEMI INCOMPLETE}  ${BAD REQUEST}          ${INCOMPLETE DATA ERROR}

Post Member Should Pass
    [Template]  Post Member Should Pass
#   ${DATA}                     ${RESPONSE STATUS}
    ${AKIN ADEYEMI JSON}      ${CREATED}

#----------------------

Get Member Should Pass
    [Template]  Get Member Should Pass
#     ${ID}            ${RESPONSE STATUS}              ${EXPECTED RESPONSE}
        ${MEMBER ID}              ${OK}                 ${MEMBER ACCOUNT ID}

Get Member Should Fail
    [Template]  Get Member Should Fail
#     ${ID}            ${RESPONSE STATUS}              ${EXPECTED RESPONSE}
       400             ${NOT FOUND}                     ${MEMBER NOT FOUND ERROR}
       what            ${BAD REQUEST}                   ${INVALID PATH PARAMETRE}



*** Keywords ***

Post Member Should Pass
    [Arguments]         ${DATA}             ${RESPONSE STATUS}
    Post Member         ${DATA}
    Should Be True      ${STATUS}==${RESPONSE STATUS}
    ${EXPECTED}=        To Json             ${DATA}
    Get Member Should Pass  ${BODY.json()}[mid]        ${OK}        ${EXPECTED}[account_id]
    Run Keyword If      ${BODY.json()}[mid] > 1       Set Suite Variable      ${MEMBER ID}        ${BODY.json()}[mid]
    Run Keyword If      ${BODY.json()}[mid] > 1       Set Suite Variable      ${MEMBER ACCOUNT ID}        ${BODY.json()}[account_id]

Post Member Should Fail
    [Arguments]                         ${DATA}                     ${RESPONSE STATUS}                  ${EXPECTED RESPONSE}
    Post Member                         ${DATA}
    Should Be True                      ${STATUS}==${RESPONSE STATUS}
    Should Be Equal As Strings          ${BODY.json()}              ${EXPECTED RESPONSE}

Post Member
    [Arguments]         ${DATA}
    Create Session      esusu                ${URL}
    ${RESPONSE}=        Post Request         esusu              /members/add/       data=${DATA}           headers=&{HEADERS DEFAULT}
    Set Test Variable       ${STATUS}               ${RESPONSE.status_code}
    Set Test Variable       ${BODY}                 ${RESPONSE}

# ------------------------------

Get Member Should Pass
    [Arguments]                     ${ID}            ${RESPONSE STATUS}              ${EXPECTED RESPONSE}
    Get Member                      ${ID}
    Should Be True                  ${STATUS}==${RESPONSE STATUS}
    Should Be Equal As Strings      ${BODY.json()}[account_id]              ${EXPECTED RESPONSE}

Get Member Should Fail
    [Arguments]                     ${ID}              ${RESPONSE STATUS}              ${EXPECTED RESPONSE}
    Get Member                      ${ID}
    Should Be True                  ${STATUS}==${RESPONSE STATUS}
#    Log to console                  ${BODY.json()}
#    Should Be Equal As Strings      ${BODY.json()}      ${EXPECTED RESPONSE}

Get Member
    [Arguments]          ${ID}
    Create Session         esusu            ${URL}
    ${RESPONSE}=            Get Request     esusu               /member/${ID}                  headers=&{HEADERS DEFAULT}
    Set Test Variable       ${STATUS}       ${RESPONSE.status_code}
    Set Test Variable       ${BODY}         ${RESPONSE}

#-------------------------



