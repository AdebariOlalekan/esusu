--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.14
-- Dumped by pg_dump version 11.4 (Debian 11.4-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: ubanquity
--

CREATE TABLE public.groups (
    gid integer NOT NULL,
    name character varying,
    contribution_amount integer NOT NULL,
    group_size integer NOT NULL,
    creation_date timestamp with time zone DEFAULT now(),
    round_duration integer NOT NULL,
    creator character varying NOT NULL,
    account_number character varying(10) NOT NULL
);


ALTER TABLE public.groups OWNER TO ubanquity;

--
-- Name: groups_gid_seq; Type: SEQUENCE; Schema: public; Owner: ubanquity
--

CREATE SEQUENCE public.groups_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_gid_seq OWNER TO ubanquity;

--
-- Name: groups_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ubanquity
--

ALTER SEQUENCE public.groups_gid_seq OWNED BY public.groups.gid;


--
-- Name: members; Type: TABLE; Schema: public; Owner: ubanquity
--

CREATE TABLE public.members (
    mid integer NOT NULL,
    group_id integer NOT NULL,
    account_id character varying(10) NOT NULL,
    customer_id character varying NOT NULL,
    name character varying,
    rating double precision,
    date_added timestamp with time zone DEFAULT now()
);


ALTER TABLE public.members OWNER TO ubanquity;

--
-- Name: members_mid_seq; Type: SEQUENCE; Schema: public; Owner: ubanquity
--

CREATE SEQUENCE public.members_mid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.members_mid_seq OWNER TO ubanquity;

--
-- Name: members_mid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ubanquity
--

ALTER SEQUENCE public.members_mid_seq OWNED BY public.members.mid;


--
-- Name: groups gid; Type: DEFAULT; Schema: public; Owner: ubanquity
--

ALTER TABLE ONLY public.groups ALTER COLUMN gid SET DEFAULT nextval('public.groups_gid_seq'::regclass);


--
-- Name: members mid; Type: DEFAULT; Schema: public; Owner: ubanquity
--

ALTER TABLE ONLY public.members ALTER COLUMN mid SET DEFAULT nextval('public.members_mid_seq'::regclass);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: ubanquity
--



--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: ubanquity
--



--
-- Name: groups_gid_seq; Type: SEQUENCE SET; Schema: public; Owner: ubanquity
--

SELECT pg_catalog.setval('public.groups_gid_seq', 1, false);


--
-- Name: members_mid_seq; Type: SEQUENCE SET; Schema: public; Owner: ubanquity
--

SELECT pg_catalog.setval('public.members_mid_seq', 1, false);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ubanquity
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (gid);


--
-- Name: members members_pkey; Type: CONSTRAINT; Schema: public; Owner: ubanquity
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (mid);


--
-- Name: members members_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ubanquity
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(gid) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

